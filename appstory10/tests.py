from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.core import mail
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .views import home, formstory10, checkemail, createuser
from .models import Form
import time

# Create your tests here.


class Story10TestCase(TestCase):
    def test_url_landing_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/form/')
        self.assertEqual(response.status_code, 200)

    def test_function_landing(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, home)

    def test_function_form(self):
        found = resolve('/form/')
        self.assertEqual(found.func, formstory10)
    def test_template(self):
        response = Client().get('/form/')
        self.assertTemplateUsed(response, 'form.html')
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'home.html')

    def test_send_email(self):
        mail.send_mail(
            'Subject here', 'Here is the message.',
            'from@example.com', ['to@example.com'],
            fail_silently=False,
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
        
    def test_add_existing_email(self):
        Form.objects.create(email='asd@asd', name='asd@asd', password='asd@asd')
        response = Client().get('/checkemail'+ 'asd@asd')
        html_response = response.content.decode('utf8')
        self.assertIn("Not Found", html_response)
    
    def test_check_email(self):
        email = 'asd@asd'
        found = resolve('/checkemail/' + email)
        self.assertEqual(found.func, checkemail)
