$(document).on('submit','#new_user_form',function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: '/form/',
    data:{
      email:$('email').val(),
      name:$('name').val(),
      password:$('password').val(),
      csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
    }, 
    success:function(){
      alert("Created New User!")
    }
  });
}