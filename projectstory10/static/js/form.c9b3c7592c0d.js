var func = function () {
  if ($('#email').val() != '') {
    url = '/checkemail/' + $('#email').val();

    $("#submitBtn").prop('disabled', true);
    // console.log(url);
    $.ajax({
      url: url,
      success: function (response) {
        if (response.substring(4) == "NotAvailable") {
          $('#alert').addClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
          $('#alert').html("Email sudah terdaftar");
          $("#submitBtn").prop('disabled', true);
        } else if ($('#username').val() != "" && $('#password').val() != "" && $('#email').val() != "") {
          $('#alert').removeClass('alert alert-danger col-md-6 offset-md-3 col-10 offset-1');
          $('#alert').html("");
          $("#submitBtn").prop('disabled', false);
        }
      }
    });
  }
  if ($('#username').val() == "" || $('#password').val() == "" || $('#email').val() == "") {
    $("#submitBtn").prop('disabled', true);
  }
}
$('#formstory10').on('submit', function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: '/form/',
        data: {
          email: $('email').val(),
          name: $('name').val(),
          password: $('password').val(),
          csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function () {
          alert("Created New User!")
        }
      });
    }